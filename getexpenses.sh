#!/bin/bash

# Get Gab expenses. Run as cron daily (> 1600h) or 2-3 times per day.
#
# NOTE: Requires the "curl" command.
#
# USAGE: getexpenses.sh >> expenses.log from inside a cron job, example:
#
# 0 */8 * * * ~/scripts/getexpenses.sh >> ~/expenses.log
#
# Consume the resulting log using the Node "gabexpenses" command.
# See project's README.md for details.
#
# (c) 2021 operator9.

JSON=$(curl https://gab.com/api/v1/expenses 2>/dev/null)
echo "${JSON:0:-1},\"timestamp\":\"$(date)\"}"
