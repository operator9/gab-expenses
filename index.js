#!/usr/bin/env node
/*
 *  Gab expenses
 *
 *  Copyright (c) 2021 operator9
 */

'use strict';

const fs = require('fs');
const Path = require('path');
const options = require('commander')
  .option('-p, --parse <file>', 'Log file to parse into database')
  .option('-e, --export <file.csv>', 'Export as CSV file. Use "-" for STDOUT. Default previous month. See --month.')
  .option('-m, --month <[M[M[M[M]]]]|yyyy-MM>', 'Month to export.')
  .parse(process.argv);

const args = options.opts();

if ( args.parse ) {
  parse(args.parse);
}
else if ( args.export ) {
  exportCSV(args.export, args.month);
}
else options.help();

function exportCSV(file, month) {
  month = getMonth(month);
  if ( month ) {
    const db = getDB();
    if ( !db ) return;

    const csv = [ 'date;progress' ];
    csv.push(...db.prepare('SELECT * FROM progress WHERE SUBSTR(date,0,8) = ? ORDER BY date').all(month).map(e => `${ e.date };${ e.progress }`));
    try {
      if ( file === '-' ) {
        process.stdout.write(csv.join('\r\n'));
      }
      else {
        fs.writeFileSync(file, csv.join('\r\n'), 'utf8');
      }
    }
    catch(err) {
      console.error(`Could not write data to file: "${ file }"`);
    }
  }
  else console.log('Invalid date.');
}

function parse(file) {
  const fs = require('fs');
  const entries = [];
  try {
    entries.push(...fs.readFileSync(file, 'utf-8')
      .replace(/\r/gm, '')
      .split('\n')
      .filter(e => e.length)
      .map(e => {
        const json = JSON.parse(e);
        json.timestamp = (new Date(json.timestamp))
          .toISOString()
          .replace('T', ' ')
          .substr(0, 19);
        return json;
      })
    );
  }
  catch(err) {
    console.log(`Could not parse this expenses.log file: "${ err.message }"`);
    return;
  }

  const db = getDB();
  if ( !db ) return;

  const insert = db.prepare('INSERT INTO expenses (date, progress) VALUES (?, ?)');

  db.transaction(() => {
    entries.forEach(e => {
      try {
        insert.run(e.timestamp, e.expenses);
      }
      catch {}
    });
  })(entries);

  db.close();
}

function getDB() {
  try {
    const db = require('better-sqlite3')(Path.join(__dirname, 'expenses.db'));
    db.prepare('CREATE TABLE IF NOT EXISTS "expenses" ("date" TEXT NOT NULL UNIQUE, "progress" REAL DEFAULT 0, PRIMARY KEY("date"))').run();
    db.prepare('CREATE VIEW IF NOT EXISTS progress AS SELECT SUBSTR(date,0,11) date, IIF(SUBSTR(date,9,2)=01, MIN(progress),MAX(progress)) progress FROM expenses GROUP BY SUBSTR(date,0,11)')
      .run();
    return db;
  }
  catch(err) {
    console.error(`Could not open or create a database: ${ err.message }`);
    return null;
  }
}

function getMonth(m) {
  const months = [ 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december' ];
  let d = new Date;
  d.setDate(2);

  try {
    if ( !m ) {
      d.setMonth(d.getMonth() - 1);
    }
    else if ( !isNaN(m) ) {
      if ( m < 1 || m > 12 ) return console.error('Month out of range (1-12).');
      d.setMonth(m - 1);
    }
    else if ( isNaN(m) ) {
      const i = months.map(e => e.substr(0, m.length)).indexOf(m.toLowerCase());
      if ( i > -1 ) {
        d.setMonth(i);
      }
      else {
        d = new Date(m);
      }
    }
  }
  catch {return null;}

  return d.toISOString().substr(0, 7);
}
