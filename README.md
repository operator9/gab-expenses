Gab Expenses Progress Bar
=========================

Package containing script meant to be run daily as cron that will connect to Gab's API end-point to
extract their "operational cost" progress bar.

This repository is not associated with Gab or any affiliations.

Data for some months is included.

Install
-------

Make sure to have Node.js installed (v.14 or newer).

Clone the repository:

    $ git clone https://codeberg.org/operator9/gab-expenses.git

OR use SSH:

    $ git clone git@codeberg.org:operator9/gab-expenses.git

CD into the project folder and install the `gabexpense` command using:

    $ npm i -g .

Copy the `expenses.sh` to a folder of your choice.

Usage
-----

Edit for example a cron job (`crontab -e`) and add a line to run the
`expenses.sh` script once a day (best time is after 1600h EST), or 2 or 3 times (i.e. every 12 or 8
hours):

    0 */8 * * * ~/scripts/getexpenses.sh >> ~/expenses.log

Once a month is closed consume and compile the generated log file using:

    gabexpenses --parse ~/expenses.log

You can now export the new data as CSV for the previously closed month using:

    gabexpenses --export month.csv
    gabexpenses --export -            # exports to STDOUT

You can export any month using the `-m, --month` option:

    gabexpenses -e month.csv -m jul
    gabexpenses -e month.csv -m 7
    gabexpenses -e month.csv -m 2021-07

Import CSV into Excel or Calc to generate graphs.

Use `gabexpenses -h` for details.

Note
----

Some data in the monthly folder have been manually collected before this software was created, and
thus some days are missing before July/2021.


Database
--------

project folder -> expenses.db (SQLite).

You can log as many data-points per day as you want.

There are only two fields in the main table `expenses`:

- `date` (text)
- `progress` (real number aka floating point)

The VIEW `progress` contains the same two fields, but only one data-point per day with the maximum
value for that day and no time, only the date.

License
-------

GPL 3.0

(c) 2021 operator9